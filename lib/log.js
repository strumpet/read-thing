// This file is part of SBS.
//
// read-thing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// read-thing is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with read-thing.  If not, see
// <http://www.gnu.org/licenses/>.

const moment = require('moment')
const util = require('util')

module.exports = {
  error: function (error) {
    const t = moment().format('YYYY-MM-DD HH:mm:ss')

    if (error instanceof Error) {
      if (error.message) {
        const msg = `${t}: ${util.inspect(error)}`
        if (error.moreInfo)
          console.error(`${msg} (${error.moreInfo})`)
        else
          console.error(msg)
      }
      else {
        console.error(`${t}: ${util.inspect(error)}`)
      }
      console.error(error.stack)
    }
    else {
      console.error(`${t}: ${util.inspect(error)}`)
    }
  },
  info: function (message) {
    const t = moment().format('YYYY-MM-DD HH:mm:ss')
    console.log(`${t}: ${message}`)
  }
};
