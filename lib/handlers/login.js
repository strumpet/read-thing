const OAuth = require('oauth').OAuth;
const o_config = require('../../config/oauth.js');
const oa = new OAuth(
  o_config.request_url,
  o_config.access_url,
  o_config.client_key,
  o_config.client_secret,
  o_config.version,
  o_config.callback_url,
  o_config.method
);
const log = require('../log.js')

module.exports = function (req, res) {
  // Twitter doesn't use the oauth_token_secret
  // http://stackoverflow.com/a/9857158
  oa.getOAuthRequestToken((err, requestToken, _requestSecret, results) => {
    if (err) {
      log.error(err)
      return res.sendStatus(500)
    }

    return res.redirect(
      `https://api.twitter.com/oauth/authenticate?oauth_token=${requestToken}`
    )
  });
}
