// Copyright 2022 Alex Dunn <dunn.alex@gmail.com>
//
// This file is part of read-thing.
//
// read-thing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// read-thing is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with read-thing.  If not, see
// <http://www.gnu.org/licenses/>.

if (process.env.NODE_ENV !== "production")
  require('dotenv').config()

const conf = require('../../config/config.js')

const fs = require('fs');
const ejs = require('ejs')
const busboy = require('busboy');

const cm = require('commonmark')
const reader = new cm.Parser()
const writer = new cm.HtmlRenderer({ smart: true, safe: true })

const archive = require('../archive.js');
const log = require('../log.js');

module.exports = function (req, res) {
  if (req.headers['content-length'] > 25000)
    return res.sendStatus('400')

  const twitterID = req.cookies.get(
    'twitter_id',
    { signed: true, secure: (process.env.NODE_ENV === 'production') }
  );

  if (twitterID === undefined)
    return res.sendStatus('400')

  let text = '',
      newname = '',
      duplicate = false,
      overwrite = false

  const boy = busboy({ headers: req.headers })

  boy.on('file', (fieldname, file, info) => {
    const { filename } = info

    newname = filename
      .replace(/\ /g, '-')
      .replace(/\.[^\.]+$/, '')

    duplicate = fs.existsSync(`${conf.APP_ROOT}/shared/data/${twitterID}/${newname}`)

    file.on('data', (chunk) => {
      text += chunk.toString('utf-8')
    });
  });

  boy.on('field', (fieldname, val, _info) => {
    if (fieldname === 'overwrite') {
      overwrite = (val === 'on')
    }

    return
  })

  boy.on('close', () => {
    if (duplicate && !overwrite) {
      res.status(403)
      const flash = { mode: 'warning', message: 'A text with that name already exists.' }
      return res.render(
        `${conf.APP_ROOT}/views/signed-in`,
        {
          archive: archive(twitterID),
          description: 'Upload yr Markdown for reading',
          flash: flash,
          title: 'Read A Thing',
          twitterID: twitterID
        }
      )
    }

    try {
      fs.mkdirSync(`${conf.APP_ROOT}/shared/data/${twitterID}`);
    } catch (err) {
      // Likely the error is just the directory already existing,
      // so log it and continue
      log.error(err)
    }

    const content = writer.render(reader.parse(text))
    // https://github.com/mde/ejs/blob/54d5ab0b3fe1e00998f6000b8caa7fb633512290/examples/functions.js#L17
    const output = ejs.compile(
      fs.readFileSync(`${conf.APP_ROOT}/views/compiled.ejs`, 'utf8'),
      { filename: newname }
    )({
      description: content.slice(0, 100),
      flash: null,
      rendered: content,
      title: newname
    })

    return fs.writeFile(`${conf.APP_ROOT}/shared/data/${twitterID}/${newname}`, output, (err) => {
      if (err) {
        log.error(err)
        return res.sendStatus('500')
      }

      return res.redirect(303, `/${twitterID}/${newname}`)
    })
  })
  return req.pipe(boy)
}
