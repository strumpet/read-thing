// Copyright 2022 Alex Dunn <dunn.alex@gmail.com>
//
// This file is part of read-thing.
//
// read-thing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// read-thing is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with read-thing.  If not, see
// <http://www.gnu.org/licenses/>.

if (process.env.NODE_ENV !== "production")
  require('dotenv').config()

const archive = require('../archive.js')
const conf = require('../../config/config.js')

module.exports = function (req, res) {
  const twitterID = req.cookies.get(
    'twitter_id',
    { signed: true, secure: (process.env.NODE_ENV === 'production') }
  );

  if (twitterID)
    return res.render(
      `${conf.APP_ROOT}/views/signed-in`,
      {
        archive: archive(twitterID),
        description: 'Upload yr Markdown for reading',
        flash: null,
        title: 'Read A Thing',
        twitterID: twitterID
      }
    )

  return res.render(
    `${conf.APP_ROOT}/views/signed-out`,
    {
      flash: null,
      title: 'Read A Thing',
      description: 'Upload yr Markdown for reading'
    }
  )
}
