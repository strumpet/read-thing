# a basic pastebin for markdown

Sign in with Twitter, upload a file; it’s converted to HTML by
CommonMark and saved to `/your-twitter-id/file-name`.  That’s it.

## running locally

Create a Twitter app and set the environment variables `OAUTH_CLIENT_KEY` and `OAUTH_CLIENT_SEC`.

```shell
npm install
node main.js # now running at localhost:3333
```

# license

Copyright 2022 Alex Dunn

read-thing is free software: you can redistribute it and/or modify it
under the terms of the GNU Affero General Public License as published
by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

read-thing is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with read-thing.  If not, see
<http://www.gnu.org/licenses/>.
