FROM node:20-alpine

RUN addgroup --gid 10001 --system reader && \
  adduser --uid 10000 --system \
  --ingroup reader --home /home/reader reader

RUN apk add --no-cache tini

RUN mkdir -p /home/reader/thing
WORKDIR /home/reader/thing

COPY . .
RUN npm i

RUN chown -R reader:reader /home/reader

USER reader
ENTRYPOINT ["/sbin/tini", "--", "node", "main.js"]
