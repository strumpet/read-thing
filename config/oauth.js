module.exports = {
  access_url: "https://api.twitter.com/oauth/access_token",
  callback_url: process.env.CALLBACK,
  client_key: process.env.OAUTH_CLIENT_KEY,
  client_secret: process.env.OAUTH_CLIENT_SEC,
  method: "HMAC-SHA1",
  request_url: "https://api.twitter.com/oauth/request_token",
  version: "1.0A"
}
