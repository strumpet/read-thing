module.exports = {
  PORT: process.env.PORT || '3333',
  APP_ROOT: process.env.APP_ROOT,
}
