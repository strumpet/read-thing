// Copyright 2022 Alex Dunn <dunn.alex@gmail.com>
//
// This file is part of read-thing.
//
// read-thing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// read-thing is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with read-thing.  If not, see
// <http://www.gnu.org/licenses/>.

(function(){
  const submit = document.querySelector('input[type=submit]');
  if (!submit) return;
  submit.disabled = true;

  const maxSize = document.querySelector('.max-size');

  function checkSize(e) {
    console.log(e.target.files);
    if (e.target.files[0].size < 25000) {
      submit.disabled = false;
      maxSize[0].classList.remove('error');
    } else {
      submit.disabled = true;
      maxSize[0].classList.add('error');
    }
  }

  const filepicker = document.querySelector('input[type=file]');
  if (filepicker) {
    filepicker.addEventListener('change', checkSize, false);
  }
})();
