// Copyright 2022 Alex Dunn <dunn.alex@gmail.com>
//
// This file is part of read-thing.
//
// read-thing is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// read-thing is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public
// License along with read-thing.  If not, see
// <http://www.gnu.org/licenses/>.

if (process.env.NODE_ENV !== "production")
  require('dotenv').config()

const conf = require('./config/config.js')

const express = require('express')
const app = express()
app.use(express.static(`${conf.APP_ROOT}/public`))
app.use(
  express.static(
    `${conf.APP_ROOT}/shared/data`,
    { setHeaders: (res, path, stat) => { res.setHeader('Content-Type', 'text/html') } }
  )
)

// https://expressjs.com/en/advanced/best-practice-security.html
const helmet = require('helmet')
app.use(helmet())

// Cookies
// https://github.com/pillarjs/cookies/blob/73d4ba8b707de7e8b33d9c6c3d01402ddd7eade9/test/express.js#L16
const Keygrip = require('keygrip');
const cookies = require('cookies').express;
const crypto = require('crypto');
const grip = Keygrip([crypto.randomBytes(10).toString('base64')]);
// Otherwise the cookies module will complain "Cannot send secure
// cookie over unencrypted connection" since behind nginx nothing is
// encrypted
app.set('trust proxy', true)
app.use(cookies(grip))

// template engine
app.set('view engine', 'ejs');

const handle = require('./lib/handlers.js')
app.get('/', handle.root)
app.get(/^\/login\/?$/, handle.login)
app.get(/^\/auth\/?$/, handle.oauth)
app.post('/upload', handle.upload)

app.listen(conf.PORT, () => {
  console.log(`Listening on port ${conf.PORT}`)
})
